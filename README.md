<h1 align="center">Aiken Quiz Creator</h1>
<h3 align="center">Uma ferramenta leve, simples e intuitiva</h3>

<p align="center">
  <a href="#-sobre-o-projeto">Sobre o projeto</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-usando-modelo">Usando modelo</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-recursos">Recursos</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-como-contribuir">Como contribuir</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-tecnologias">Tecnologias</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
</p>

## 💡 Sobre o projeto

Este é um projeto de código aberto que serve como uma alternativa gratuita a ferramentas comerciais de mesmo propósito. 

É uma ferramente de lado cliente que processo os dados de entrada gerando uma saída formatada para importação de questões no Moodle utilizando Aiken Format.

- Leve
- Gratuito
- Fácil

Veja o projeto funcionando [aqui](https://jrrsouzaj.gitlab.io/aiken-quizz-creator).

## 🌠 Recursos

- Permite adicionar quantas questões quiser
- Avisa quando algum elemento obrigatório não foi fornecido
- Exporta já no formato necessário para importação no Moodle

## 🚀 Usando modelo

Se você instalou o git você pode clonar o código para sua máquina, ou baixar um ZIP de todos os arquivos diretamente.
[Faça o download do ZIP deste local](https://gitlab.com/JRRSouzaJ/aiken-quizz-creator/-/archive/main/sobre-mim-main.zip), ou execute o seguinte comando [git](https://git-scm.com/downloads) para clonar os arquivos em sua máquina:
```bash
git clone https://gitlab.com/JRRSouzaJ/aiken-quizz-creator.git
```
- Assim que os arquivos estiverem em sua máquina, abra a pasta "aiken-quizz-creator" no seu editor preferido e 'mãos a obra'. Modifique o código como desejar para atender às suas necessidades.

## 🤔 Como contribuir

- Faça um fork deste repositório;
- Crie uma branch com seus recursos: `git checkout -b my-feature`;
- Confirme suas mudanças: `git commit -m "feat: my new feature"`;
- Envie para sua branch: `git push origem my-feature`.

Uma vez que seu pedido de pull tenha sido fundido (merge), você pode apagar sua branch.

## 🔧 Tecnologias

Este projeto usa as segintes tecnologias:

- [HTML5] - Para estrutura do site
- [CSS] - Para estilo visual
- [JavaScript] - Para as intereções com a interface
- [JQuery] - Para o processamento dos dados

## 📝 Licença

Este projeto está sob a licença do MIT. Veja o arquivo [LICENSE](LICENSE.md) para mais detalhes.

[//]: # (Referências utilizadas)

   [jQuery]: <http://jquery.com>
   [JavaScript]: <https://www.javascript.com>
   [HTML5]: <https://html.spec.whatwg.org/>
   [CSS]: <https://www.w3.org/Style/CSS>
